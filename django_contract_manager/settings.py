import os

import sentry_sdk
from decouple import config
from django.utils.translation import gettext_lazy as _
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# All settings common to all environments
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
PROJECT_NAME = os.path.basename(PROJECT_ROOT)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config("SECRET_KEY", cast=str)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config("DEBUG", default=False, cast=bool)

# Master Data Defaults
MDAT_ROOT_CUSTOMER_ID = config("MDAT_ROOT_CUSTOMER_ID", default=1, cast=int)

SESSION_ENGINE = "user_sessions.backends.db"

SILENCED_SYSTEM_CHECKS = [
    "captcha.recaptcha_test_key_error",
    "admin.E410",
    "fields.E003",
]

SESSION_COOKIE_SECURE = not DEBUG

CSRF_COOKIE_SECURE = not DEBUG

SESSION_COOKIE_HTTPONLY = True

CSRF_COOKIE_HTTPONLY = True

SESSION_COOKIE_NAME = "__Secure-sessionid" if not DEBUG else "sessionid"

CSRF_COOKIE_NAME = "__Secure-csrftoken" if not DEBUG else "csrftoken"

ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = [
    "127.0.0.1",
]
CORS_ORIGIN_ALLOW_ALL = True

WATCHMAN_TOKENS = config("WATCHMAN_TOKENS", default=SECRET_KEY, cast=str)

APPEND_SLASH = False

TASTYPIE_ALLOW_MISSING_SLASH = True
TASTYPIE_ABSTRACT_APIKEY = True

# Application definition
INSTALLED_APPS = [
    # WhiteNoise - static file handling
    "whitenoise.runserver_nostatic",
    # CORS headers
    "corsheaders",
    # Debug Toolbar
    "debug_toolbar",
    # Django monitoring
    "watchman",
    # Minify,
    "django_minify_html",
    # multi-language
    "parler",
    # Django modules
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "user_sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # LDAP Auth modules
    "django_python3_ldap",
    # Celery task
    "django_celery_results",
    "django_celery_beat",
    "flower",
    # API modules
    "tastypie",
    "django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth",
    # Template
    "django_template_limitless.django_template_limitless",
    # External modules
    "django_session_ldap_attributes.django_session_ldap_attributes",
    # client modules
    "django_cdstack_client.django_cdstack_client",
    "django_cloudstack_client.django_cloudstack_client",
    "django_datacenter_manager_client.django_datacenter_manager_client",
    "django_devops_backend_client.django_devops_backend_client",
    "django_directory_backend_client.django_directory_backend_client",
    "django_dyndns_client.django_dyndns_client",
    "django_iotstack_client.django_iotstack_client",
    "django_ispstack_client.django_ispstack_client",
    "django_master_data_backend_client.django_master_data_backend_client",
    "django_monstack_client.django_monstack_client",
    "django_pki_manager_client.django_pki_manager_client",
    "django_security_backend_client.django_security_backend_client",
    "django_smsgw_client.django_smsgw_client",
    "django_ucstack_client.django_ucstack_client",
    "django_voipstack_client.django_voipstack_client",
    "django_whstack_client.django_whstack_client",
    "django_wifistack_client.django_wifistack_client",
    # External datasource support modules
    "django_sap_business_one_client.django_sap_business_one_client",
    # Contract Manager modules
    "django_contract_billing.django_contract_billing",
    "django_contract_factoring.django_contract_factoring",
]

# Dynamic loading of MDAT modules
for name in os.listdir(PROJECT_ROOT + "/.."):
    if os.path.isdir(name) and name.startswith("django_mdat_"):
        module_name = name + "." + name
        INSTALLED_APPS.append(module_name)

# Dynamic loading of Contract Manager modules
for name in os.listdir(PROJECT_ROOT + "/.."):
    if os.path.isdir(name) and name.startswith("django_contract_manager_"):
        module_name = name + "." + name
        INSTALLED_APPS.append(module_name)

MIGRATION_MODULES = {
    "django_mdat_customer_details": None,
    "django_mdat_location": None,
}

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.gzip.GZipMiddleware",
    "django_minify_html.middleware.MinifyHtmlMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "user_sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "x_forwarded_for.middleware.XForwardedForMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

AUTHENTICATION_BACKENDS = ("django_python3_ldap.auth.LDAPBackend",)

STATICFILES_STORAGE = "whitenoise.storage.CompressedStaticFilesStorage"

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

ROOT_URLCONF = "django_contract_manager.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

LANGUAGES = (
    ("en", _("English")),
    ("de", _("German")),
)

PARLER_LANGUAGES = {
    1: (
        # Default SITE_ID, all languages
        {
            "code": "en",
        },
        {
            "code": "de",
        },
    ),
    "default": {
        # This is applied to each entry in this setting:
        "hide_untranslated": False,
        "hide_untranslated_menu_items": False,
    },
}

# Celery
CELERY_RESULT_BACKEND = "django-db"
DJANGO_CELERY_BEAT_TZ_AWARE = False
CELERY_BROKER_URL = config("CELERY_BROKER_URL", default="amqp://", cast=str)
CELERY_TASK_DEFAULT_QUEUE = "django_contract_manager"

WSGI_APPLICATION = "django_contract_manager.wsgi.application"

# Sentry
SENTRY_DSN = config("SENTRY_DSN", default="sentry_dsn", cast=str)
if not DEBUG and not SENTRY_DSN == "sentry_dsn":
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        send_default_pii=True,
        traces_sample_rate=0.1,
        integrations=[DjangoIntegration(), CeleryIntegration()],
    )

# CONTRACT MANAGER
CONTRACT_MANAGER_BILLING_DAYS_IN_FUTURE = config(
    "CONTRACT_MANAGER_BILLING_DAYS_IN_FUTURE", default=2, cast=int
)

# CDSTACK API
CDSTACK_API_URL = config(
    "CDSTACK_API_URL", default="https://cdstack-api.example.com/", cast=str
)
CDSTACK_API_USER = config("CDSTACK_API_USER", default="user", cast=str)
CDSTACK_API_PASSWD = config("CDSTACK_API_PASSWD", default="secret", cast=str)

# CLOUDSTACK API
CLOUDSTACK_API_URL = config(
    "CLOUDSTACK_API_URL", default="https://cloudstack-api.example.com/", cast=str
)
CLOUDSTACK_API_USER = config("CLOUDSTACK_API_USER", default="user", cast=str)
CLOUDSTACK_API_PASSWD = config("CLOUDSTACK_API_PASSWD", default="secret", cast=str)

# DATACENTER MANAGER API
DATACENTER_MANAGER_API_URL = config(
    "DATACENTER_MANAGER_API_URL",
    default="https://datacenter-manager-api.example.com/",
    cast=str,
)
DATACENTER_MANAGER_API_USER = config(
    "DATACENTER_MANAGER_API_USER", default="user", cast=str
)
DATACENTER_MANAGER_API_PASSWD = config(
    "DATACENTER_MANAGER_API_PASSWD", default="secret", cast=str
)

# DEVOPS BACKEND API
DEVOPS_BACKEND_API_URL = config(
    "DEVOPS_BACKEND_API_URL",
    default="https://devops-backend-api.example.com/",
    cast=str,
)
DEVOPS_BACKEND_API_USER = config("DEVOPS_BACKEND_API_USER", default="user", cast=str)
DEVOPS_BACKEND_API_PASSWD = config(
    "DEVOPS_BACKEND_API_PASSWD", default="secret", cast=str
)

# DIRECTORY BACKEND API
DIRECTORY_BACKEND_API_URL = config(
    "DIRECTORY_BACKEND_API_URL",
    default="https://directory-backend-api.example.com/",
    cast=str,
)
DIRECTORY_BACKEND_API_USER = config(
    "DIRECTORY_BACKEND_API_USER", default="user", cast=str
)
DIRECTORY_BACKEND_API_PASSWD = config(
    "DIRECTORY_BACKEND_API_PASSWD", default="secret", cast=str
)

# DYNDNS API
DYNDNS_API_URL = config(
    "DYNDNS_API_URL", default="https://dyndns-api.example.com/", cast=str
)
DYNDNS_API_USER = config("DYNDNS_API_USER", default="user", cast=str)
DYNDNS_API_PASSWD = config("DYNDNS_API_PASSWD", default="secret", cast=str)

# IOTSTACK API
IOTSTACK_API_URL = config(
    "IOTSTACK_API_URL", default="https://iotstack-api.example.com/", cast=str
)
IOTSTACK_API_USER = config("IOTSTACK_API_USER", default="user", cast=str)
IOTSTACK_API_PASSWD = config("IOTSTACK_API_PASSWD", default="secret", cast=str)

# ISPSTACK API
ISPSTACK_API_URL = config(
    "ISPSTACK_API_URL", default="https://ispstack-api.example.com/", cast=str
)
ISPSTACK_API_USER = config("ISPSTACK_API_USER", default="user", cast=str)
ISPSTACK_API_PASSWD = config("ISPSTACK_API_PASSWD", default="secret", cast=str)

# MASTER DATA BACKEND API
MASTER_DATA_BACKEND_API_URL = config(
    "MASTER_DATA_BACKEND_API_URL",
    default="https://master-data-backend-api.example.com/",
    cast=str,
)
MASTER_DATA_BACKEND_API_USER = config(
    "MASTER_DATA_BACKEND_API_USER", default="user", cast=str
)
MASTER_DATA_BACKEND_API_PASSWD = config(
    "MASTER_DATA_BACKEND_API_PASSWD", default="secret", cast=str
)

# MONSTACK API
MONSTACK_API_URL = config(
    "MONSTACK_API_URL", default="https://monstack-api.example.com/", cast=str
)
MONSTACK_API_USER = config("MONSTACK_API_USER", default="user", cast=str)
MONSTACK_API_PASSWD = config("MONSTACK_API_PASSWD", default="secret", cast=str)

# PKI MANAGER API
PKI_MANAGER_API_URL = config(
    "PKI_MANAGER_API_URL", default="https://pki-manager-api.example.com/", cast=str
)
PKI_MANAGER_API_USER = config("PKI_MANAGER_API_USER", default="user", cast=str)
PKI_MANAGER_API_PASSWD = config("PKI_MANAGER_API_PASSWD", default="secret", cast=str)

# SECURITY BACKEND API
SECURITY_BACKEND_API_URL = config(
    "SECURITY_BACKEND_API_URL",
    default="https://security-backend-api.example.com/",
    cast=str,
)
SECURITY_BACKEND_API_USER = config(
    "SECURITY_BACKEND_API_USER", default="user", cast=str
)
SECURITY_BACKEND_API_PASSWD = config(
    "SECURITY_BACKEND_API_PASSWD", default="secret", cast=str
)

# SMSGW API
SMSGW_API_URL = config(
    "SMSGW_API_URL", default="https://smsgw-api.example.com/", cast=str
)
SMSGW_API_USER = config("SMSGW_API_USER", default="user", cast=str)
SMSGW_API_PASSWD = config("SMSGW_API_PASSWD", default="secret", cast=str)

# UCSTACK API
UCSTACK_API_URL = config(
    "UCSTACK_API_URL", default="https://ucstack-api.example.com/", cast=str
)
UCSTACK_API_USER = config("UCSTACK_API_USER", default="user", cast=str)
UCSTACK_API_PASSWD = config("UCSTACK_API_PASSWD", default="secret", cast=str)

# VOIPSTACK API
VOIPSTACK_API_URL = config(
    "VOIPSTACK_API_URL", default="https://voipstack-api.example.com/", cast=str
)
VOIPSTACK_API_USER = config("VOIPSTACK_API_USER", default="user", cast=str)
VOIPSTACK_API_PASSWD = config("VOIPSTACK_API_PASSWD", default="secret", cast=str)

# WHSTACK API
WHSTACK_API_URL = config(
    "WHSTACK_API_URL", default="https://whstack-api.example.com/", cast=str
)
WHSTACK_API_USER = config("WHSTACK_API_USER", default="user", cast=str)
WHSTACK_API_PASSWD = config("WHSTACK_API_PASSWD", default="secret", cast=str)

# WIFISTACK API
WIFISTACK_API_URL = config(
    "WIFISTACK_API_URL", default="https://wifistack-api.example.com/", cast=str
)
WIFISTACK_API_USER = config("WIFISTACK_API_USER", default="user", cast=str)
WIFISTACK_API_PASSWD = config("WIFISTACK_API_PASSWD", default="secret", cast=str)

# CLOUDBLUE MARKETPLACE
CLOUDBLUE_MARKETPLACE_API_URL = config(
    "CLOUDBLUE_MARKETPLACE_API_URL", default="http://localhost/api", cast=str
)
CLOUDBLUE_MARKETPLACE_API_USER = config(
    "CLOUDBLUE_MARKETPLACE_API_USER", default="username", cast=str
)
CLOUDBLUE_MARKETPLACE_API_PASSWD = config(
    "CLOUDBLUE_MARKETPLACE_API_PASSWD", default="invalid", cast=str
)
CLOUDBLUE_MARKETPLACE_API_KEY = config(
    "CLOUDBLUE_MARKETPLACE_API_KEY", default="invalid", cast=str
)

# ALSO MARKETPLACE
ALSO_MARKETPLACE_API_URL = config(
    "ALSO_MARKETPLACE_API_URL",
    default="http://localhost/SimpleAPI/SimpleAPIService.svc/rest",
    cast=str,
)
ALSO_MARKETPLACE_API_USER = config(
    "ALSO_MARKETPLACE_API_USER", default="username", cast=str
)
ALSO_MARKETPLACE_API_PASSWD = config(
    "ALSO_MARKETPLACE_API_PASSWD", default="invalid", cast=str
)

# SAP CONNECTION
SBO_COMPANY_LICENSE_SERVER = config(
    "SBO_COMPANY_LICENSE_SERVER", default="SAP:30000", cast=str
)
SBO_COMPANY_DATABASE_VERSION = config(
    "SBO_COMPANY_DATABASE_VERSION", default=15, cast=int
)
SBO_COMPANY_DATABASE_NAME = config(
    "SBO_COMPANY_DATABASE_NAME", default="DEMO", cast=str
)
SBO_COMPANY_DATABASE_HOSTNAME = config(
    "SBO_COMPANY_DATABASE_HOSTNAME", default="SAP", cast=str
)
SBO_COMPANY_DATABASE_TRUSTED = config(
    "SBO_COMPANY_DATABASE_TRUSTED", default=False, cast=bool
)
SBO_COMPANY_USER = config("SBO_COMPANY_USER", default="manager", cast=str)
SBO_COMPANY_PASSWORD = config("SBO_COMPANY_PASSWORD", default="secret", cast=str)

# Database
# https://docs.djangoproject.com/en/stable/ref/settings/#databases

# MAIN DATABASE
MAIN_DATABASE_NAME = config("MAIN_DATABASE_NAME", default="maindb", cast=str)
MAIN_DATABASE_USER = config("MAIN_DATABASE_USER", default="maindb", cast=str)
MAIN_DATABASE_PASSWD = config("MAIN_DATABASE_PASSWD", default="secret", cast=str)
MAIN_DATABASE_HOST = config("MAIN_DATABASE_HOST", default="127.0.0.1", cast=str)
MAIN_DATABASE_PORT = config("MAIN_DATABASE_PORT", default="3306", cast=str)

# MDAT DATABASE
MDAT_DATABASE_NAME = config("MDAT_DATABASE_NAME", default="maindb", cast=str)
MDAT_DATABASE_USER = config("MDAT_DATABASE_USER", default="maindb", cast=str)
MDAT_DATABASE_PASSWD = config("MDAT_DATABASE_PASSWD", default="secret", cast=str)
MDAT_DATABASE_HOST = config("MDAT_DATABASE_HOST", default="127.0.0.1", cast=str)
MDAT_DATABASE_PORT = config("MDAT_DATABASE_PORT", default="3306", cast=str)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": MAIN_DATABASE_NAME,
        "USER": MAIN_DATABASE_USER,
        "PASSWORD": MAIN_DATABASE_PASSWD,
        "HOST": MAIN_DATABASE_HOST,
        "PORT": MAIN_DATABASE_PORT,
        "OPTIONS": {"init_command": "SET sql_mode='STRICT_TRANS_TABLES'"},
    },
    "mdat": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": MDAT_DATABASE_NAME,
        "USER": MDAT_DATABASE_USER,
        "PASSWORD": MDAT_DATABASE_PASSWD,
        "HOST": MDAT_DATABASE_HOST,
        "PORT": MDAT_DATABASE_PORT,
        "OPTIONS": {"init_command": "SET sql_mode='STRICT_TRANS_TABLES'"},
    },
}

DATABASE_ROUTERS = ["django_contract_manager.dbrouters.ContractManagerRouter"]

# SMTP / EMAIL
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = config("EMAIL_HOST", default="127.0.0.1", cast=str)
EMAIL_USE_TLS = config("EMAIL_USE_TLS", default=False, cast=bool)
EMAIL_USE_SSL = config("EMAIL_USE_SSL", default=True, cast=bool)
EMAIL_PORT = config("EMAIL_PORT", default=465, cast=int)
EMAIL_HOST_USER = config("EMAIL_HOST_USER", default="user@example.com", cast=str)
EMAIL_HOST_PASSWORD = config("EMAIL_HOST_PASSWORD", default="secret", cast=str)

# Password validation
# https://docs.djangoproject.com/en/stable/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# LDAP server type
LDAP_SERVER_TYPE = config("LDAP_SERVER_TYPE", default="OpenLDAP", cast=str)

# The URL of the LDAP server.
LDAP_AUTH_URL = config("LDAP_AUTH_URL", default="ldaps://localhost:636", cast=str)

# Initiate TLS on connection.
LDAP_AUTH_USE_TLS = config("LDAP_AUTH_USE_TLS", default=True, cast=bool)

# The LDAP search base for looking up users.
LDAP_AUTH_SEARCH_BASE = config(
    "LDAP_AUTH_SEARCH_BASE", default="ou=people,dc=example,dc=com", cast=str
)

# The LDAP class that represents a user.
LDAP_AUTH_OBJECT_CLASS = config(
    "LDAP_AUTH_OBJECT_CLASS", default="inetOrgPerson", cast=str
)

# User model fields mapped to the LDAP
# attributes that represent them.
if LDAP_SERVER_TYPE == "ActiveDirectory":
    LDAP_AUTH_USER_FIELDS = {
        "username": "userPrincipalName",
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }
elif LDAP_SERVER_TYPE == "OpenLDAP":
    LDAP_AUTH_USER_FIELDS = {
        "username": "uid",
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }
else:
    LDAP_AUTH_USER_FIELDS = {
        "username": "uid",
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }

# A tuple of django model fields used to uniquely identify a user.
LDAP_AUTH_USER_LOOKUP_FIELDS = ("username",)

# Path to a callable that takes a dict of {model_field_name: value},
# returning a dict of clean model data.
# Use this to customize how data loaded from LDAP is saved to the User model.
LDAP_AUTH_CLEAN_USER_DATA = "django_python3_ldap.utils.clean_user_data"

# Path to a callable that takes a user model and a dict of {ldap_field_name: [value]},
# and saves any additional user relationships based on the LDAP data.
# Use this to customize how data loaded from LDAP is saved to User model relations.
# For customizing non-related User model fields, use LDAP_AUTH_CLEAN_USER_DATA.
LDAP_AUTH_SYNC_USER_RELATIONS = "django_session_ldap_attributes.django_session_ldap_attributes.ldap_func.sync_user_relations"

# Path to a callable that takes a dict of {ldap_field_name: value},
# returning a list of [ldap_search_filter]. The search filters will then be AND'd
# together when creating the final search filter.
LDAP_AUTH_FORMAT_SEARCH_FILTERS = "django_python3_ldap.utils.format_search_filters"

# Path to a callable that takes a dict of {model_field_name: value}, and returns
# a string of the username to bind to the LDAP server.
# Use this to support different types of LDAP server.
if LDAP_SERVER_TYPE == "ActiveDirectory":
    LDAP_AUTH_FORMAT_USERNAME = (
        "django_python3_ldap.utils.format_username_active_directory_principal"
    )
elif LDAP_SERVER_TYPE == "OpenLDAP":
    LDAP_AUTH_FORMAT_USERNAME = "django_python3_ldap.utils.format_username_openldap"
else:
    LDAP_AUTH_FORMAT_USERNAME = "django_python3_ldap.utils.format_username_openldap"

# Sets the login domain for Active Directory users.
LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN = config(
    "LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN", default=None, cast=str
)

if LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN == "None":
    LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN = None

# The LDAP username and password of a user for querying the LDAP database for user
# details. If None, then the authenticated user will be used for querying, and
# the `ldap_sync_users` command will perform an anonymous query.
LDAP_AUTH_CONNECTION_USERNAME = config(
    "LDAP_AUTH_CONNECTION_USERNAME", default=None, cast=str
)
LDAP_AUTH_CONNECTION_PASSWORD = config(
    "LDAP_AUTH_CONNECTION_PASSWORD", default=None, cast=str
)

# Set connection/receive timeouts (in seconds) on the underlying `ldap3` library.
LDAP_AUTH_CONNECT_TIMEOUT = None
LDAP_AUTH_RECEIVE_TIMEOUT = None

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = config("LANGUAGE_CODE", default="en", cast=str)

TIME_ZONE = config("TIME_ZONE", default="UTC", cast=str)

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
