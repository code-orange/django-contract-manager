class ContractManagerRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label == "django_mdat_customer_details":
            return "mdat"

        if model._meta.app_label == "django_mdat_location":
            return "mdat"

        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == "django_mdat_customer_details":
            return "mdat"

        if model._meta.app_label == "django_mdat_location":
            return "mdat"

        return None

    def allow_relation(self, obj1, obj2, **hints):
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return None
