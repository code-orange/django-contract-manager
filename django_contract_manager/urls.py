from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path
from django.http import HttpResponseRedirect
from django.views.generic import RedirectView

from django_contract_manager_api.django_contract_manager_api import urls as api_urls

urlpatterns = [
    path("", RedirectView.as_view(url="/contract/dashboard/", permanent=True)),
    path("dashboard", RedirectView.as_view(url="/contract/dashboard/", permanent=True)),
    path("accounts/", include("django.contrib.auth.urls")),
    path("i18n/", include("django.conf.urls.i18n")),
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("api/", include(api_urls)),
    re_path(
        r"^contract/",
        include("django_contract_manager_main.django_contract_manager_main.urls"),
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
